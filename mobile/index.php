<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>

<head>
    <?php require_once('./includes/head.php'); ?>

<body>



    <?php require_once('./includes/topo.php'); ?>



    <!-- slider -->
    <div class="container">
        <div class="row slider-index telefone-topo1">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators navs-personalizados">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/banner-1.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/banner-2.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/banner-3.jpg" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/banner-4.jpg" alt="">
                    </div>

                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
        </div>
    </div>
    <!-- slider -->

<?php  ?>


    <div class="container">
        <div class="row">
            
            
            <?php
                $result = $obj_site->select("tb_produtos", "order by rand() limit 8 ");
                if(mysql_num_rows($result) > 0)
                {   
                   ?>
                    <div class="col-xs-12 produtos-home pbottom10">
                        <div class="top25">
                            <div class="col-xs-1">
                                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/carrinho-compra.png" alt="">
                            </div>
                            <div class="col-xs-6">
                                <h1>PRODUTOS</h1>
                            </div>
                        </div>
                    </div>
                   <?php

                        while($row = mysql_fetch_array($result))
                        {
                            $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
                            $row_categoria = mysql_fetch_array($result_categoria);
                            ?>
                            <div class="col-xs-6 text-center top20">
                            <div class="descricao-produtos-geral">
                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
                                    <h1><?php Util::imprime( Util::troca_value_nome($row[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ) ?></h1>
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="" class="input98" />
                                </a>
                                <p><?php Util::imprime($row[titulo]); ?></p>

                                <?php if ($row[preco] > 0): ?>
                                    <span class="preco">R$ <?php echo Util::formata_moeda($row[preco]); ?></span>
                                <?php else: ?>
                                    <span class="preco">&nbsp;</span>
                                <?php endif; ?>

                                <div class="clearfix"></div>

                                <!-- botao-azul -->
                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-primary btn-azul-home1" role="button">Ver Mais</a>
                                <!-- botao-azul <-->

                            </div>
                        </div>
                            <?php

                            

                        }

                  
                }
                ?>


        </div>
    </div>







    <!-- produtos -->
    <div class="container">
        <div class="row top25">
            <div class="col-xs-12 produtos-home pbottom10">
                <div class="">
                    <div class="col-xs-1">
                        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/carrinho-compra.png" alt="">
                    </div>
                    <div class="col-xs-6">
                        <h1>SERVIÇOS</h1>
                    </div>
                </div>
            </div>
        </div>




        <!-- ITEM 01 -->
        <div class="row top20">

            <?php
            $result = $obj_site->select("tb_servicos", "order by rand() limit 6");
            if(mysql_num_rows($result) > 0)
            {
                while($row = mysql_fetch_array($result))
                {
                    ?>
                    <div class="col-xs-6 ">
                        <div class="thumbnail servicos-descricao">
                            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]); ?>">
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 210, 135, array('class'=>'')) ?>

                                <div class="caption">
                                    <h3><?php Util::imprime($row[titulo]); ?></h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>

    </div>
    <!-- produtos -->



    <!-- portifolio-servico -->
    <div class="container top30 bottom50">
        <div class="row">
            <div class="col-xs-12">
                <div class="servicos-home">
                    <h5><i class="fa fa-star"></i>PRODUTOS</h5>
                </div>
            </div>
        </div>




        <!-- descricao -->
        <?php
        $result = $obj_site->select("tb_categorias_produtos","ORDER BY RAND() LIMIT 3");
        if(mysql_num_rows($result) > 0)
        {
            while($row = mysql_fetch_array($result))
            {
                ?>
                <div class="row">
                    <div class="col-xs-12 servicos-home1 top20">
                        <div class="">
                            <div class="col-xs-4">
                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?categoria=<?php Util::imprime($row[0]) ?>">
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 127, 80, array('class'=>'')) ?>
                                </a>
                            </div>
                            <div class="col-xs-8">
                                <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?categoria=<?php Util::imprime($row[0]) ?>">
                                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                                    <p><?php Util::imprime($row[descricao], 180); ?></p>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <!-- descricao -->


        <div class="text-right">
            <!-- botao-azul -->
            <a href="./servicos" class="btn btn-primary btn-azul-home" role="button">Ver Todos</a>
            <!-- botao-azul -->
        </div>

    </div>
    <!-- portifolio-servico -->


    <!-- conheca mais a disklimp -->
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="servicos-home">
                    <h5><i class="fa fa-university"></i>A EMPRESA</h5>
                </div>
            </div>
        </div>

        <div class="row top5">
            <div class="col-xs-12 index-conheca-disklimp text-right">
                <?php $dados = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
                <a href="./empresa">
                    <img src="./imgs/banner-empresa-home.jpg" height="171" width="450" alt="">
                    <p><?php Util::imprime($dados[descricao], 500) ?></p>
                </a>
                <!-- botao-azul -->
                <a href="./empresa" class="btn btn-primary btn-azul-home" role="button">Ver Mais</a>
                <!-- botao-azul -->

            </div>
        </div>
    </div>
    <!-- conheca mais a disklimp -->


    <!-- DICAS -->
    <div class="container">
        <div class="row top35">
            <div class="col-xs-12">
                <div class="servicos-home">
                    <h5><i class="fa fa-sticky-note"></i>Dicas</h5>
                </div>
            </div>
        </div>

        <!-- dica 01 -->
        <?php
        $result = $obj_site->select("tb_dicas","AND imagem <> '' LIMIT 3");
        if(mysql_num_rows($result) > 0)
        {
            while($row = mysql_fetch_array($result))
            {
                ?>
                <div class="row">
                    <div class="col-xs-12 servicos-home1">
                        <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas/<?php Util::imprime($row[url_amigavel]); ?>">
                            <div class="">
                                <div class="col-xs-4">
                                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 130, 82, array('class'=>'')) ?>
                                </div>
                                <div class="col-xs-8">
                                    <h1><?php Util::imprime($row[titulo]); ?></h1>
                                    <p><?php Util::imprime($row[descricao], 300); ?></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <!-- dica 01 -->







        <div class="text-right">
            <!-- botao-azul -->
            <a href="./dicas" class="btn btn-primary btn-azul-home" role="button">Ver Todos</a>
            <!-- botao-azul -->
        </div>


    </div>
    <!-- DICAS -->





    <?php require_once('./includes/rodape.php'); ?>


</body>

</html>

