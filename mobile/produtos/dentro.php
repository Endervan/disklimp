<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// INTERNA DE PRODUTOS
$url = $_GET[get1];


if(!empty($url))
{
	$complemento = "AND url_amigavel = '$url'";
}


$result = $obj_site->select("tb_produtos",$complemento);
if(mysql_num_rows($result)==0)
{
	Util::script_location(Util::caminho_projeto()."/mobile/produtos/");
}

$dados_dentro = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!doctype html>
<html>
<head>
	<?php require_once('../includes/head.php'); ?>

	<!-- FlexSlider -->
	<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
	<!-- Syntax Highlighter -->
	<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
	<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
	<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

	<!-- Optional FlexSlider Additions -->
	<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
	<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
	<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


	<!-- Syntax Highlighter -->
	<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


	<!-- Demo CSS -->
	<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


	<script>
		$(window).load(function() {
          // The slider being synced must be initialized first
          $('#carousel').flexslider({
          	animation: "slide",
          	controlNav: false,
          	animationLoop: false,
          	slideshow: false,
          	itemWidth: 170,
          	itemMargin: 5,
          	asNavFor: '#slider'
          });

          $('#slider').flexslider({
          	animation: "slide",
          	controlNav: false,
          	animationLoop: false,
          	slideshow: false,
          	sync: "#carousel"
          });
      });
	</script>


</head>



<body>


	<?php require_once('../includes/topo.php'); ?>




	<!-- bg-internas-->
	<div class="container">
		<div class="row">
			<div class="bg-produtos"></div>
		</div>
	</div>
	<!-- bg-internas-->


	<!-- produtos usando drop -->
	<div class="container">
		<div class="row top20">
			<div class="col-xs-12 ">
				<a href="javascript: window.history.go(-1)" class="btn btn-primary">
					VOLTAR
				</a>
			</div>
		</div>
	</div>



	<!--menu produtos geral-->
	<div class="container top30">

		<div class="row">
			<div class="col-xs-12">
				<div class="produtos-dentro-descricao">
					<h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>
				</div>
			</div>


			<div class="top20 bottom20">
				<?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 480, 300, array('class'=>'input100')) ?>
			</div>



			<div class="col-xs-12 produtos-dentro-descricao">
				<p><?php Util::imprime($dados_dentro[descricao]) ?></p>

				<a href="javascript:void(0);" class="btn btn-azul-contatos top20" title="" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
		          ADICIONAR AO ORÇAMENTO
		        </a>
        
			</div>


			</div>
		</div>
		<!--menu produtos geral-->


		<!-- veja tambem -->
		<div class="container">
			<div class="row">
				<div class="col-xs-12 top20">
					<div class="veja-tambem-produtos">
						<h2>Veja Também</h2>

					</div>
				</div>
				<!--lista de produtos geral-->
				<?php
				$result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 2");
	            if(mysql_num_rows($result) > 0)
	            {
	                while($row = mysql_fetch_array($result))
	                {
	                    ?>
						<div class="col-xs-6 text-center top20">
							<div class="descricao-produtos-geral">
								<a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
									<h1><?php Util::imprime( Util::troca_value_nome($row[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ) ?></h1>
									<img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="" class="input98" />
								</a>
								<p><?php Util::imprime($row[titulo]); ?></p>

								<!-- botao-azul -->
								<a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn-primary btn-azul-home1" role="button">Ver Mais</a>
								<!-- botao-azul <-->

							</div>
						</div>
						<?php
					}
				}
				?>
				<!--lista de produtos geral-->



			</div>
		</div>
		<!-- veja tambem -->


		<!-- produtos usando drop -->


		<!-- rodape -->
		<?php require_once('../includes/rodape.php') ?>
		<!-- rodape -->

	</body>
	</html>
