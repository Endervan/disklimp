<div class="container topo">


    <!-- menu -->
    <div class="row bg-topo">
        <div class="col-xs-6 top5">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="Home">
                <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home">
            </a>
        </div>
        <div class="col-xs-6">
            <select class="menu-home input100 top25" id="menu-site">
                <div class="menu-home1"></div>
                <option value="">ABRIR MENU </option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile">HOME</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa/">A EMPRESA</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/categorias/">PRODUTOS</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/categorias-servicos/">SERVIÇOS</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/dicas/">DICAS</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/">ORÇAMENTO</option>
                <option value="<?php echo Util::caminho_projeto() ?>/mobile/contatos/">CONTATOS</option>
            </select>
        </div>
    </div>
    <!-- menu -->

    <!-- telefone -->
    <div class="row telefone-topo">
        <div class="col-xs-12 ">
        <div class="col-xs-5 text-right">
                <h3>TELEFONE</h3>
                <h4 class=""><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>
                    <?php Util::imprime($config[telefone1]) ?>
                </h4>
            </div>
            <div class="col-xs-5 top10 pbottom10">
                <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>" class="btn btn-cinza" title="CHAMAR">
                    CHAMAR
                </a>
            </div>
        </div>
    </div>
    <!-- telefone -->

</div>
