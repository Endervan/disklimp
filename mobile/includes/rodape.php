<div class="clearfix"></div>
<div class="container top40">
	<div class="row bg-fundo-preto">
		<div class=" col-xs-9 text-right">
			<h4>© Copyright DiskLimp</h4>	
		</div>

		<div class="col-xs-3 text-right top5">
			
			<?php if ($config[google_plus] != "") { ?>
				<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" class="pull-left top15 left20">
					<p style="color: #fff"><i class="fa fa-google-plus"></i></p>
				</a>
			<?php } ?>


			<a href="http://www.homewebbrasil.com.br/" title="Homeweb Marketing" target="_blank" class="left20">
	            <img src="<?php echo Util::caminho_projeto() ?>/images/logo_homeweb.png" alt="" height="50">
	        </a>
		</div>
		
	</div>
	
</div>

