<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
    <?php require_once('../includes/head.php'); ?>

</head>


</head>

<body>


    <?php require_once('../includes/topo.php'); ?>




    <!-- bg-internas-->
    <div class="container">
        <div class="row">
            <div class="bg-contatos"></div>
        </div>
    </div>
    <!-- bg-internas-->


    <!-- contatos -->
    <div class="container ">
        <div class="row bottom20 top40 ">

            <div class="col-xs-12">
                <div class="menu-contatos">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs text-center " role="tablist">
                        <li role="presentation" class="active"><a href="#"  ><i class="fa fa-envelope"></i>FALE CONOSCO</a></li>
                        <li role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco"  ><i class="fa fa-child"></i>TRABALHE CONOSCO</a></li>
                        <li role="presentation"><a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos" ><i class="fa fa-calculator"></i>ORÇAMENTOS</a></li>
                    </ul>
                </div>
            </div>

        </div>
        <!-- CONTATOS -->
        <div class="row">
            <div class=" col-xs-12 contatos-descricao">
                <div class="col-xs-6"><h3><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span><?php Util::imprime($config[telefone1]) ?></h3></div>
                <div class="col-xs-4">
                    <div class=" top10">
                        <a href="tel:+55<?php Util::imprime($config[telefone1]) ?>">
                            <button type="submit" class="btn btn-cinza-contatos1" name="CHAMAR">
                                CHAMAR
                            </button>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <!-- CONTATOS -->

        <!-- ENDERENCO -->
        <div class="row">
            <div class=" contatos-descricao">
                <div class="col-xs-9">
                    <div class="col-xs-2 top40">
                        <span class="glyphicon glyphicon-home btn-lg" aria-hidden="true"></span>
                    </div>
                    <div class="col-xs-10">
                        <h3><?php Util::imprime($config[endereco]) ?></h3>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class=" top10">
                        <a href="">
                            <button type="submit" class="btn btn-cinza-contatos1" name="CHAMAR">
                                MAPA
                            </button>
                        </a>
                    </div>
                </div>

            </div>
        </div>
        <!-- ENDERENCO -->

        <!-- Tab panes -->
        <div class="tab-content">


            <!-- orcamentos -->
            <div role="tabpanel" class="tab-pane fade in active" id="messages">



                <?php
                //  VERIFICO SE E PARA ENVIAR O EMAIL
                if(isset($_POST[nome]))
                {

                  $texto_mensagem = "
                                      Nome: ".($_POST[nome])." <br />
                                      Email: ".($_POST[email])." <br />
                                      Telefone: ".($_POST[telefone])." <br />
                                      Assunto: ".($_POST[assunto])." <br />
                                      Mensagem: <br />
                                      ".$mensagem = (nl2br($_POST[mensagem]))."
                                      ";

                    if(Util::envia_email($config[email],utf8_decode("Contato pelo site ").$_SERVER[SERVER_NAME], $texto_mensagem, utf8_decode($nome_remetente), $email)); 
                      Util::script_msg("Obrigado por entrar em contato.");
                      unset($_POST);
                }
                ?>


                <form class="form-inline FormContato top20 pbottom20" role="form" method="post" enctype="multipart/form-data">

                    <div class="row">
                        <div class="col-xs-6 form-group ">
                            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                            <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                        </div>
                        <div class="col-xs-6 form-group">
                            <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                            <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 top20 form-group">
                            <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                            <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                        </div>
                        <div class="col-xs-6 top20 form-group">
                            <label class="glyphicon glyphicon-home"> <span>Assunto</span></label>
                            <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
                        </div>

                    </div>




                    <div class="row">
                        <div class="col-xs-12 top20 form-group">
                            <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                            <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                        </div>

                    </div>

                    <div class="clearfix"></div>

                    <div class="text-right top30">
                        <button type="submit" class="btn btn-azul-contatos" name="btn_trabalhe_conosco">
                            ENVIAR<span class="glyphicon glyphicon-ok">
                            </button>
                        </div>

                    </form>


                </div>
                <!-- orcamentos -->

            </div>
            <!-- Tab panes -->
            <!-- mapa-geral -->
            <!--  <div class="mapa-contatos">

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3837.6217181778134!2d-48.01083799999997!3d-15.876467000000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a326fb9b303ed%3A0x6fd1705b0f2fad84!2sAlkha!5e0!3m2!1spt-BR!2sbr!4v1442685372964" width="475" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div> -->
        <!-- mapa-geral -->


    </div>
    <!-- contatos -->


    <!-- rodape -->
    <?php require_once('../includes/rodape.php') ?>
    <!-- rodape -->

</body>
</html>

<script>
$(document).ready(function() {
    $('.FormContato').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {

                    },
                    emailAddress: {
                        message: 'Esse endereço de email não é válido'
                    }
                }
            },
            telefone: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            assunto: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            mensagem: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    });
});
</script>



<script>
$(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {

                    },
                    emailAddress: {
                        message: 'Esse endereço de email não é válido'
                    }
                }
            },
            telefone: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            assunto: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            escolaridade: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            cargo: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            area: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            curriculo: {
                validators: {
                    notEmpty: {
                        message: 'Por favor insira seu currículo'
                    },
                    file: {
                        extension: 'doc,docx,pdf,rtf',
                        type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                        maxSize: 5*1024*1024,   // 5 MB
                        message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                    }
                }
            },
            mensagem: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    });
});
</script>



<script>
$(document).ready(function() {
    $('.FormOrcamento').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            nome: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {

                    },
                    emailAddress: {
                        message: 'Esse endereço de email não é válido'
                    }
                }
            },
            telefone: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            empresa: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            area: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            cidade: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            servico: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            estado: {
                validators: {
                    notEmpty: {

                    }
                }
            },
            curriculo: {
                validators: {
                    notEmpty: {
                        message: 'Por favor insira seu currículo'
                    },
                    file: {
                        extension: 'doc,docx,pdf,rtf',
                        type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                        maxSize: 5*1024*1024,   // 5 MB
                        message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                    }
                }
            },
            mensagem: {
                validators: {
                    notEmpty: {

                    }
                }
            }
        }
    });
});
</script>
