﻿<?php
require_once("../class/Include.class.php"); 
require_once('../class/recaptchalib.php');
$obj_site = new Site();




?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="contato">
		
		<div class="titulos">
			<h3>CONTATOS</h3>
			<h2>FALE CONOSCO</h2>
		</div>
		
		<div class="content_navegacao_forms">
			<ul>
				<li><a href="<?php echo Util::caminho_projeto() ?>/contato/" title="FALE CONOSCO" class="fale_conosco">FALE CONOSCO</a></li>
				<li><a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco" title="TRABALHE CONOSCO" class="trabalhe_conosco">TRABALHE CONOSCO</a></li>
				<li><a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ORÇAMENTOS" class="orcamentos">ORÇAMENTOS</a></li>
			</ul>
		</div>
		
		<div class="clear">&nbsp;</div>
		
		<?php  
		//	VERIFICO SE E PARA ENVIAR O EMAIL
		if(isset($_POST[nome]))
		{
			
			if (!Util::valida_captcha()) {
					Util::script_msg(utf8_decode("Por favor, digite o que você vê na imagem"));
					Util::script_go_back();
				} else {
					$nome_remetente = ($_POST[nome]);
					$assunto = "Contato pelo site $_SERVER[SERVER_NAME]";
					$email = ($_POST[email]);
					$mensagem = nl2br($_POST[mensagem]);
					$telefone = ($_POST[telefone]);
					
					$texto_mensagem = "
									  Nome: $nome_remetente <br />
									  Assunto: $assunto <br />
									  Telefone: $telefone <br />
									  Email: $email <br />
									  Mensagem:	<br />
									  $mensagem
									  ";


										Util::envia_email($config[email], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
                                        Util::script_msg("Obrigado por entrar em contato.");



					
					

                                        
                    
				}
		}
		?>
		
		<form id="form_contato" name="form_contato" method="post" action="#">
            
			<input type="text" name="nome" id="nome" title="Campo Nome" placeholder="SEU NOME" required="required" />
			<input type="text" name="email" id="email" title="Campo E-mail" placeholder="SEU E-MAIL" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" />
			<input type="text" name="telefone" id="telefone" title="Campo Telefone" placeholder="SEU TELEFONE" required="required" />
			
			<textarea name="mensagem" rows="5" id="mensagem" title="Campo Mensagem" placeholder="SUA MENSAGEM" required="required"></textarea>
			
			<div style="float:left;">
				<label>*Digite o que você vê na imagem abaixo.</label>
				<?php Util::gera_captcha("white"); ?>
			</div>
			
			<input type="submit" name="enviar" id="enviar" value="Enviar" />
		</form>
		
		
		
		<div class="content_mapa">
			<h2 class="titulo_mapa">SAIBA COMO CHEGAR</h2>
			
			<iframe width="940" height="310" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.br/maps?q=<?php Util::imprime($config[latitude]); ?>,<?php Util::imprime($config[longitude]); ?>&amp;num=1&amp;ie=UTF8&amp;t=m&amp;ll=<?php Util::imprime($config[latitude]); ?>,<?php Util::imprime($config[longitude]); ?>&amp;spn=0.02548,0.080595&amp;z=14&amp;output=embed"></iframe>
			
		</div>
		
		
		
		
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>