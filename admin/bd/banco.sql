﻿-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.54-rel12.6-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

--
-- Definition of table `tb_grupos_logins`
--

DROP TABLE IF EXISTS `tb_grupos_logins`;
CREATE TABLE `tb_grupos_logins` (
  `idgrupologin` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `imagem` varchar(45) DEFAULT NULL,
  `exibir_menu` varchar(3) DEFAULT 'SIM',
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  PRIMARY KEY (`idgrupologin`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_grupos_logins`
--

/*!40000 ALTER TABLE `tb_grupos_logins` DISABLE KEYS */;
INSERT INTO `tb_grupos_logins` (`idgrupologin`,`nome`,`imagem`,`exibir_menu`,`ativo`) VALUES 
 (1,'Administradores',NULL,'SIM','SIM'),
 (9,'Contabilidade',NULL,'SIM','SIM');
/*!40000 ALTER TABLE `tb_grupos_logins` ENABLE KEYS */;


--
-- Definition of table `tb_grupos_logins_tb_paginas`
--

DROP TABLE IF EXISTS `tb_grupos_logins_tb_paginas`;
CREATE TABLE `tb_grupos_logins_tb_paginas` (
  `id_grupologin` int(11) NOT NULL,
  `id_pagina` int(11) NOT NULL,
  PRIMARY KEY (`id_grupologin`,`id_pagina`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_grupos_logins_tb_paginas`
--

/*!40000 ALTER TABLE `tb_grupos_logins_tb_paginas` DISABLE KEYS */;
INSERT INTO `tb_grupos_logins_tb_paginas` (`id_grupologin`,`id_pagina`) VALUES 
 (1,148),
 (1,149),
 (1,150),
 (1,151),
 (1,152),
 (1,153),
 (1,154),
 (1,155),
 (1,156),
 (1,157),
 (1,158),
 (10,148),
 (10,149),
 (10,150),
 (10,151),
 (10,152),
 (10,153),
 (10,154),
 (10,155),
 (10,156),
 (10,157),
 (10,158);
/*!40000 ALTER TABLE `tb_grupos_logins_tb_paginas` ENABLE KEYS */;


--
-- Definition of table `tb_logins`
--

DROP TABLE IF EXISTS `tb_logins`;
CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`idlogin`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_logins`
--

/*!40000 ALTER TABLE `tb_logins` DISABLE KEYS */;
INSERT INTO `tb_logins` (`idlogin`,`nome`,`senha`,`ativo`,`id_grupologin`,`email`) VALUES 
 (1,'Marcio André da Silva','e10adc3949ba59abbe56e057f20f883e','SIM',1,'marciomas@gmail.com'),
 (2,'David Leandro dos Santos','e10adc3949ba59abbe56e057f20f883e','SIM',1,'design.davidleandro@gmail.com'),
 (3,'Angela','e10adc3949ba59abbe56e057f20f883e','SIM',1,'atendimento.sites@homewebbrasil.com.br'),
 (4,'Mas Midia','e10adc3949ba59abbe56e057f20f883e','SIM',1,'contato@masmdiaia.com.br');
/*!40000 ALTER TABLE `tb_logins` ENABLE KEYS */;


--
-- Definition of table `tb_logs_logins`
--

DROP TABLE IF EXISTS `tb_logs_logins`;
CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL AUTO_INCREMENT,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`idloglogin`)
) ENGINE=InnoDB AUTO_INCREMENT=705 DEFAULT CHARSET=utf8;


--
-- Definition of table `tb_modulos_paginas`
--

DROP TABLE IF EXISTS `tb_modulos_paginas`;
CREATE TABLE `tb_modulos_paginas` (
  `idmodulopagina` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `icone_imagem` varchar(45) DEFAULT NULL,
  `ativo` char(3) NOT NULL DEFAULT 'SIM',
  PRIMARY KEY (`idmodulopagina`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_modulos_paginas`
--

/*!40000 ALTER TABLE `tb_modulos_paginas` DISABLE KEYS */;
INSERT INTO `tb_modulos_paginas` (`idmodulopagina`,`nome`,`icone_imagem`,`ativo`) VALUES 
 (1,'Grupos',NULL,'SIM'),
 (2,'Logins',NULL,'SIM');
/*!40000 ALTER TABLE `tb_modulos_paginas` ENABLE KEYS */;


--
-- Definition of table `tb_paginas`
--

DROP TABLE IF EXISTS `tb_paginas`;
CREATE TABLE `tb_paginas` (
  `idpagina` int(11) NOT NULL AUTO_INCREMENT,
  `pagina` varchar(255) DEFAULT NULL,
  `imagem` varchar(45) DEFAULT NULL,
  `label` varchar(45) DEFAULT NULL,
  `exibir_menu` varchar(3) DEFAULT 'SIM',
  `descricao` varchar(255) DEFAULT NULL,
  `id_modulopagina` int(11) NOT NULL,
  PRIMARY KEY (`idpagina`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_paginas`
--

/*!40000 ALTER TABLE `tb_paginas` DISABLE KEYS */;
INSERT INTO `tb_paginas` (`idpagina`,`pagina`,`imagem`,`label`,`exibir_menu`,`descricao`,`id_modulopagina`) VALUES 
 (148,'/admin/grupo/altera.php',NULL,'Alterar','NAO',NULL,1),
 (149,'/admin/grupo/cadastra.php',NULL,'Cadastrar','SIM',NULL,1),
 (150,'/admin/grupo/exclui.php',NULL,'Excluir','NAO',NULL,1),
 (151,'/admin/grupo/lista.php',NULL,'Listar','SIM',NULL,1),
 (152,'/admin/grupo/permissoes.php',NULL,'Permissões','NAO',NULL,1),
 (153,'/admin/grupo/ativa_desativa.php',NULL,'Ativar e desativar','NAO',NULL,1),
 (154,'/admin/login/altera.php',NULL,'Alterar','NAO',NULL,2),
 (155,'/admin/login/cadastra.php',NULL,'Cadastrar','SIM',NULL,2),
 (156,'/admin/login/exclui.php',NULL,'Excluir','NAO',NULL,2),
 (157,'/admin/login/lista.php',NULL,'Listar','SIM',NULL,2),
 (158,'/admin/login/ativa_desativa.php',NULL,'Ativar e desativar','NAO',NULL,2);
/*!40000 ALTER TABLE `tb_paginas` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;