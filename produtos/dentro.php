﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();


// INTERNA DE PRODUTO
$url = Util::trata_dados_formulario($_REQUEST[get1]);
if(!empty($url))
{
	$complemento = "AND url_amigavel = '".$url."'";
}

$result = $obj_site->select("tb_produtos",$complemento);
$row_produto = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description($row_produto[description_google]);
$keywords = $obj_site->get_keywords($row_produto[keywords_google]);
$titulo_pagina = $obj_site->get_title($row_produto[description_google]);
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
	
	<script type="text/javascript"> 
	function mudaFoto(id)
	{
		var elemento = "#foto_"+id+" img";
		$("#img_principal a img").stop(true, true).fadeOut(500).hide(500);
		$(elemento).stop(true, true).fadeIn(1000).show(500);
	}
	</script>
	
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="produtos">
		
		<?php include("../includes/filtros_produto.php"); ?>
		
		<div id="content_holder">
			
			<?php
			$result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row_produto[id_categoriaproduto]);
			$row_categoria = mysql_fetch_array($result_categoria);
			?>
			<h2><?php Util::imprime($row_categoria[titulo]); ?></h2>
			
			<div id="galeria_produto">
				
				<div id="content_imgs_principal">
					<div id="img_principal">
						<?php
						$result = $obj_site->select("tb_galerias_produtos","AND id_produto =".$row_produto[idproduto]);
						if(mysql_num_rows($result) > 0)
						{
							while($row = mysql_fetch_array($result))
							{
								$dados[] = $row;
								?>
								<a href="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[imagem]); ?>" title="<?php Util::imprime($row_produto[titulo]); ?>" data-gallery="1" id="foto_<?php Util::imprime($row[0]); ?>">
									
									<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",381,281, array('alt'=>$row_produto[titulo])); ?>									
								
								</a>
								<?php
							}
						}
						?>
					</div>
				</div>
				
				<div id="content_tumbs">
					
					<a id="prev" class="seta_voltar" href="#">&lt;</a>
					<a id="next" class="seta_avancar" href="#">&gt;</a>
				
					<ul id="carrousel">
						
						<?php 
						if(count($dados) > 0):
						
							foreach($dados as $dado):
							?>	
								<li>
									<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(<?php echo $dado[0] ?>)">
										<?php $obj_site->redimensiona_imagem("../uploads/tumb_$dado[imagem]", 77, 57, array('alt'=>$row_produto[titulo])); ?>	
									</a>
								</li>
							<?php	
							endforeach;
							
						endif;
						?>
						
						
						
						
						<?php /*?>
						<li>
							<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(0)">
								<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_produto_galeria.jpg" alt="Carro Cuba com Estrutura Metálica" />
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(1)">
								<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_produto_galeria1.jpg" alt="Carro Cuba com Estrutura Metálica" />
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(2)">
								<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_produto_galeria2.jpg" alt="Carro Cuba com Estrutura Metálica" />
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(3)">
								<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_produto_galeria3.jpg" alt="Carro Cuba com Estrutura Metálica" />
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(4)">
								<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_produto_galeria4.jpg" alt="Carro Cuba com Estrutura Metálica" />
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(5)">
								<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_produto_galeria5.jpg" alt="Carro Cuba com Estrutura Metálica" />
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(6)">
								<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_produto_galeria6.jpg" alt="Carro Cuba com Estrutura Metálica" />
							</a>
						</li>
						<li>
							<a href="javascript:void(0)" title="Carro Cuba com Estrutura Metálica" onclick="mudaFoto(7)">
								<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_produto_galeria7.jpg" alt="Carro Cuba com Estrutura Metálica" />
							</a>
						</li>
						<?php */?>
					</ul>
				</div>
				
			</div>
			
			<div id="content_informacoes_produtos">
				
				<h2><?php Util::imprime($row_produto[titulo]); ?></h2>
				<?php /*?><p class="preco">R$ <?php echo Util::formata_moeda($row_produto[preco]); ?></p><?php */?>
				
				<?php Util::imprime($row_produto[descricao]); ?>


				<br><br>
				<a href="javascript:void(0);" class="btn-comprar" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row_produto[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row_produto[0]) ?>, 'produto'">
                    Adicionar ao orçamento
                </a>
				
			</div>
		</div>
		
		<h5><span>VEJA TAMBÉM:</span></h5>
		<ul id="lista_produtos" class="dentro">
			<?php
			$result = $obj_site->select("tb_produtos","AND url_amigavel <> '$row_produto[url_amigavel]' ORDER BY RAND() LIMIT 5");
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_array($result))
				{
					$result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
					$row_categoria = mysql_fetch_array($result_categoria);
					?>
					<li>
						<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
							<p class="categoria"><?php Util::imprime($row_categoria[titulo]); ?></p>
							<img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
							<p><?php Util::imprime($row[titulo]); ?></p>
						</a>
						<?php /*?><p class="preco">R$ <?php echo Util::formata_moeda($row[preco]); ?></p><?php */?>
						<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="leia_mais" title="Ver Mais">VER MAIS</a>
					</li>
					<?php
				}
			}
			?>
		</ul>
		
		<div class="clear">&nbsp;</div>
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>
