﻿<?php
require_once("class/Include.class.php"); 
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description("");
$keywords = $obj_site->get_keywords("");
$titulo_pagina = $obj_site->get_title("");

?>
<!doctype html>
<html>
<head>
	<?php include("includes/head.php"); ?>
		
	<!-- slider css -->
	<style>
		
		#slider .rsOverflow, #slider .rsABlock, #slider .rsSlide {overflow:visible !important;}
		
		#slider .rsTabs { width:925px; padding:0 38px; margin-top:-30px}
		#slider .rsTab {display:block; width:211px; padding:0; margin:0 10px; height:90px; float:left; background:url(<?php echo Util::caminho_projeto(); ?>/images/btn_slider.png) center center no-repeat; border:none;}
		#slider .rsTab.rsNavSelected {background:url(<?php echo Util::caminho_projeto(); ?>/images/btn_slider_hover.png) center center no-repeat; text-shadow:none; box-shadow:none;}
		#slider .rsTab:active {box-shadow:none;}
		#slider .rsTab .rsTmb{color:#515e94; font-family: 'cabinbold', sans-serif; font-size:18px; vertical-align:middle; display:table-cell; text-align:center; height:70px; width:200px;}
		
		
		
		.rsContent {color: #FFF; font-size: 24px; line-height: 32px; float: left; }
		.bContainer { position: relative; }
		.rsABlock { position: relative; display: block; left: auto; top: auto; }
		
		.blockHeadline { font-size: 42px; line-height: 50px; }
		.blockSubHeadline { font-size: 32px; line-height: 40px }
		.txtCent { text-align: center;  width: 100%; }
		
		
		@media screen and (min-width: 0px) and (max-width: 960px) { 
		  .rsContent { font-size: 22px; line-height: 28px; }
		  .blockHeadline { font-size: 32px; line-height: 32px;}
		  .blockSubHeadline { font-size: 26px; line-height: 32px }
		}
		
		@media screen and (min-width: 0px) and (max-width: 500px) { 
		  .royalSlider, .rsOverflow { height: 330px !important; }
		  .rsContent { font-size: 18px; line-height: 26px; }
		  .blockHeadline { font-size: 24px; line-height: 32px; }
		  .blockSubHeadline { font-size: 22px; line-height: 32px }
		}
	</style>
	
</head>

<body>

	<?php include("includes/topo.php"); ?>
	
	<?php include("includes/slider.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo">
		



		<?php //include("includes/chamada_produtos.php"); ?>
		


		
	
				<?php
				$result = $obj_site->select("tb_produtos", "order by rand() limit 12 ");
				if(mysql_num_rows($result) > 0)
				{	

					echo '
						<div id="chamada_produtos">
							<h3 class="titulo">PRODUTOS</h3>
							<ul id="lista_produtos">
						';

						while($row = mysql_fetch_array($result))
						{
							$result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
							$row_categoria = mysql_fetch_array($result_categoria);
							?>
							<li>
								<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php echo Util::imprime($row[titulo]); ?>">
									<p class="categoria"><?php echo Util::imprime($row_categoria[titulo]); ?></p>
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php echo Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]); ?>" />
									<p><?php Util::imprime($row[titulo]); ?></p>
								</a>
								
								<?php if ($row[preco] > 0): ?>
									<p class="preco">R$ <?php echo Util::formata_moeda($row[preco]); ?></p>	
								<?php endif ?>
								

								<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="leia_mais" title="Ver Mais">VER MAIS</a>
							</li>
							<?php
						}

					echo '
							</ul>
						</div>
						';
				}
				?>
			














		

		<div id="chamada_produtos">
			<h3 class="titulo">SERVIÇOS</h3>
		</div>

		<div id="lista_servicos">
			<ul>
				<?php
				$result = $obj_site->select("tb_categoria_servicos", "order by rand() limit 6");
				if(mysql_num_rows($result) > 0)
				{
					while($row = mysql_fetch_array($result))
					{
						?>
						<li>
							<a href="<?php echo Util::caminho_projeto(); ?>/disklimpeza/categoria/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
								<?php
								if(!empty($row[imagem]))
								{
									?>
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php echo Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
									<?php
								}
								?>
								<h5><?php Util::imprime($row[titulo]); ?></h5>
							</a>
						</li>
						<?php
					}
				}
				else
				{
					?>
					<h1>Nenhum Produto Encontrado</h1>
					<?php
				}
				?>
			</ul>
		</div>	
		





		<div class="content_coluna">
			<div class="coluna">
				<h4 class="titulo">
					<span class="icon_disklimpeza">&nbsp;</span>
					PRODUTOS
				</h4>
				
				<ul class="lista">
					<?php
					$result = $obj_site->select("tb_categorias_produtos","ORDER BY RAND() LIMIT 3");
					if(mysql_num_rows($result) > 0)
					{
						while($row = mysql_fetch_array($result))
						{
							?>
							<li>
								<a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
									<div class="content_descricao">
										<h6><?php Util::imprime($row[titulo]); ?></h6>
										<?php Util::imprime($row[descricao]); ?>
									</div>
								</a>
							</li>
							<?php
						}
					}
					?>
					</ul>
				
				<a href="<?php echo Util::caminho_projeto(); ?>/disklimpeza/" class="saiba_mais">VER TODAS</a>
				
			</div>
			
			<div class="coluna">
				<h4 class="titulo">
					<span class="icon_dicas">&nbsp;</span>
					DICAS
				</h4>
				
				<ul class="lista">
					<?php
					$result = $obj_site->select("tb_dicas","ORDER BY RAND() LIMIT 3");
					if(mysql_num_rows($result) > 0)
					{
						while($row = mysql_fetch_array($result))
						{
							?>
							<li>
								<a href="<?php echo Util::caminho_projeto(); ?>/dicas/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
									<div class="content_descricao">
										<h6><?php Util::imprime($row[titulo]); ?></h6>
										<?php Util::imprime($row[descricao]); ?>
									</div>
								</a>
							</li>
							<?php
						}
					}
					?>
				</ul>
				
				<a href="<?php echo Util::caminho_projeto(); ?>/dicas/" class="saiba_mais">VER TODAS</a>
				
			</div>
			
			<div class="coluna">
				<h4 class="titulo ">
					<span class="icon_empresa">&nbsp;</span>
					A EMPRESA
				</h4>
				
				<div class="chamada_empresa">
					<?php
					$result = $obj_site->select("tb_empresa","AND idempresa = 1");
					$row = mysql_fetch_array($result);
					?>
					<a href="" title="Empresa">
						<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[tumb_imagem]); ?>" alt="Empresa" />
						<p><?php Util::imprime($row[descricao],346); ?></p>
					</a>
				</div>
				
				<a href="<?php echo Util::caminho_projeto(); ?>/empresa/" class="saiba_mais">VER MAIS</a>
				
			</div>
		</div>
		
	</div>
	



	
	<?php include("includes/rodape.php"); ?>
	
</body>
</html>
