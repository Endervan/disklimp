﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();

$result = $obj_site->select("tb_empresa","AND idempresa = 1");
$row = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description($row[description_google]);
$keywords = $obj_site->get_keywords($row[keywords_google]);
$titulo_pagina = $obj_site->get_title($row[description_google]);
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="empresa">
		
		<h2><?php Util::imprime($row[titulo]); ?></h2>
		
		<?php Util::imprime($row[descricao]); ?>
		
		
		<div id="lista_clientes">
			
			
			<ul>
				<?php
				$result = $obj_site->select("tb_marcas_produtos","AND tumb_imagem <> ''");
				if(mysql_num_rows($result) > 0)
				{
                                    
                                    echo '<h2>NOSSOS CLIENTES</h2>';
                                    
					while($row = mysql_fetch_array($result))
					{
						?>
						<li><img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php echo Util::imprime($row[imagem]); ?>" alt="<?php echo Util::imprime($row[titulo]); ?>" /></li>
						<?php
					}
				}
				?>
			</ul>
			
		</div>
		
		
		
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>
