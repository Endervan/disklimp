// JavaScript Document

$(function() {
	
	var tamanho = 3;
	
	// aumentando a fonte
	$("#amais").click(function () {
		if(tamanho < 6)
		{
			tamanho = tamanho +1;
			var tam_data = $("#conteudo #news p").css('font-size');
			var tam_titulo = $("#news h1").css('font-size');
			var tam_leiamais = $("#news .leia_mais").css('font-size');
		
		
			tam_data = tam_data.replace('px', '');
			tam_data = parseInt(tam_data) + 1.4;
	
			$("#conteudo #news p").animate({'font-size' : tam_data + 'px'});
		
			tam_titulo = tam_titulo.replace('px', '');
			tam_titulo = parseInt(tam_titulo) + 1.4;
	
			$("#news h1").animate({'font-size' : tam_titulo + 'px'});
		
			tam_leiamais = tam_leiamais.replace('px', '');
			tam_leiamais = parseInt(tam_leiamais) + 1.4;
	
			$("#news .leia_mais").animate({'font-size' : tam_leiamais + 'px'});
		}
		
	});

	//diminuindo a fonte
	$("#amenos").click(function () {
		if(tamanho > 0)
		{
			tamanho = tamanho -1;
			
			var tam_data = $("#conteudo #news p").css('font-size');
			var tam_titulo = $("#news h1").css('font-size');
			var tam_leiamais = $("#news .leia_mais").css('font-size');
		
		
			tam_data = tam_data.replace('px', '');
			tam_data = parseInt(tam_data) - 1.4;
	
			$("#conteudo #news p").animate({'font-size' : tam_data + 'px'});
		
			tam_titulo = tam_titulo.replace('px', '');
			tam_titulo = parseInt(tam_titulo) - 1.4;
	
			$("#news h1").animate({'font-size' : tam_titulo + 'px'});
		
			tam_leiamais = tam_leiamais.replace('px', '');
			tam_leiamais = parseInt(tam_leiamais) - 1.4;
	
			$("#news.leia_mais").animate({'font-size' : tam_leiamais + 'px'});
		}
		
	});
	
});