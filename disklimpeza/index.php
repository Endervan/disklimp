﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description("");
$keywords = $obj_site->get_keywords("");
$titulo_pagina = $obj_site->get_title("");
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="disklimpeza">
		
		<h2 class="msg">
			Para dar apoio às suas necessidades do dia-a-dia a Disk Limp oferece o serviço<br />
			de terceirização do pessoal de apoio administrativo, oferecendo conforto e<br />
			tranquilidade aos seus funcionários.
		</h2>
		
		<div id="lista_servicos">
			<ul>
				<?php
				$result = $obj_site->select("tb_categoria_servicos");
				if(mysql_num_rows($result) > 0)
				{
					while($row = mysql_fetch_array($result))
					{
						?>
						<li>
							<a href="<?php echo Util::caminho_projeto(); ?>/disklimpeza/categoria/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
								<?php
								if(!empty($row[imagem]))
								{
									?>
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php echo Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
									<?php
								}
								?>
								<h5><?php Util::imprime($row[titulo]); ?></h5>
							</a>
						</li>
						<?php
					}
				}
				else
				{
					?>
					<h1>Nenhum Produto Encontrado</h1>
					<?php
				}
				?>
			</ul>
		</div>		
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>
