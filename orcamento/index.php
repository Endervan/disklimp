﻿<?php
require_once("../class/Include.class.php"); 
require_once('../class/recaptchalib.php');
$obj_site = new Site();




//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
        $id = $_GET[id];
        unset($_SESSION[solicitacoes_produtos][$id]);
        sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
        $id = $_GET[id];
        unset($_SESSION[solicitacoes_servicos][$id]);
        sort($_SESSION[solicitacoes_servicos]);
        break;
    }

}

?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="orcamento">
		
		<div class="titulos">
			<h3>CONTATOS</h3>
			<h2>ORÇAMENTOS</h2>
		</div>
		
		<div class="content_navegacao_forms">
			<ul>
				<li><a href="<?php echo Util::caminho_projeto() ?>/contato/" title="FALE CONOSCO" class="fale_conosco">FALE CONOSCO</a></li>
				<li><a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco" title="TRABALHE CONOSCO" class="trabalhe_conosco">TRABALHE CONOSCO</a></li>
				<li><a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ORÇAMENTOS" class="orcamentos">ORÇAMENTOS</a></li>
			</ul>
		</div>
		
		<div class="clear">&nbsp;</div>
		
		<?php  
		//	VERIFICO SE E PARA ENVIAR O EMAIL
		if(isset($_POST[nome]))
		{
			if (!Util::valida_captcha()) {
					Util::script_msg(utf8_decode("Por favor, digite o que você vê na imagem"));
					Util::script_go_back();

				} else {

					//  CADASTRO OS PRODUTOS SOLICITADOS
		            for($i=0; $i < count($_POST[qtd]); $i++)
		            {
		                $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

		                $mensagem_produtos .=   "
					                            <tr>
						                            <td><p>". $_POST[qtd][$i] ."</p></td>
						                            <td><p>". utf8_encode($dados[titulo]) ."</p></td>
					                            </tr>
					                            ";
		            }


		              //  CADASTRO OS SERVICOS SOLICITADOS
		              for($i=0; $i < count($_POST[qtd_servico]); $i++)
		              {         
		                  $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
		                  
		                  $mensagem_servicos .= "
		                              <tr>
		                                  <td><p>". $_POST[qtd_servico][$i] ."</p></td>
		                                  <td><p>". utf8_encode($dados[titulo]) ."</p></td>
		                               </tr>
		                              ";
		              }



					$nome_remetente = ($_POST[nome]);
					$assunto = "Contato pelo site $_SERVER[SERVER_NAME]";
					$email = ($_POST[email]);
					$mensagem = (nl2br($_POST[mensagem]));
					$telefone = ($_POST[telefone]);
					
					$empresa = ($_POST[empresa]);
					$area_atuacao = ($_POST[area_atuacao]);
					$servico = ($_POST[servico]);
					$cidade = ($_POST[cidade]);
					$estado = ($_POST[estado]);
					
					 $texto_mensagem = "
									  Nome: $nome_remetente <br />
									  Assunto: $assunto <br />
									  Telefone: $telefone <br />
									  Email: $email <br />
									  <br />
									  Empresa: $empresa<br />
									  Área de Atuação: $area_atuacao<br />
									  Serviço: $servico<br />
									  Cidade: $cidade<br />
									  Estado: $estado<br />
									  <br />
									  Mensagem:	<br />
									  $mensagem


									  <br />
							            <h2> Produtos selecionados:</h2> <br />

							            <table width='100%' border='0' cellpadding='5' cellspacing='5'>

							            <tr>
							            <td><h4>QTD</h4></td>
							            <td><h4>DESCRIÇÃO</h4></td>
							            </tr>
							            $mensagem_produtos

							            </table>



							            <br />
							            <h2> Serviços selecionados:</h2> <br />

							            <table width='100%' border='0' cellpadding='5' cellspacing='5'>

							            <tr>
							            <td><h4>QTD</h4></td>
							            <td><h4>DESCRIÇÃO</h4></td>
							            </tr>
							            $mensagem_servicos

							            </table>

									  ";
					
                 

                    if(Util::envia_email($config[email],utf8_decode("Orçamentos pelo site ").$_SERVER[SERVER_NAME], $texto_mensagem, utf8_decode($nome_remetente), $email));
                    Util::script_msg("Obrigado por entrar em contato.");
                                
                                         
				}
		}
		?>
		
		<form id="form_contato" name="form_contato" method="post" action="#">

			<?php
            if(count($_SESSION[solicitacoes_produtos]) > 0){
                ?>
                <table class="table top20 lista-itens-orcamento" width="100%">
                    <thead>
                        <tr>
                            <th align="left">ITEM</th>
                            <th align="left">DESCRIÇÃO</th>
                            <th class="text-center">QUANTIDADE</th>
                            <th class="text-center">REMOVER</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                            ?>
                            <tr>
                                <td><img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="48" width="48" alt=""></td>
                                <td><?php Util::imprime($row[titulo]) ?></td>
                                <td align="center">
                                    <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                                    <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                                </td>
                                <td align="center">
                                    <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <?php
            }
            ?>



			<?php
            if(count($_SESSION[solicitacoes_servicos]) > 0){
                ?>
                <table class="table top20 lista-itens-orcamento" width="100%">
                    <thead>
                        <tr>
                            <th align="left">ITEM</th>
                            <th align="left">DESCRIÇÃO</th>
                            <th class="text-center">QUANTIDADE</th>
                            <th class="text-center">REMOVER</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
                            $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
                            ?>
                            <tr>
                                <td><img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="48" width="48" alt=""></td>
                                <td><?php Util::imprime($row[titulo]) ?></td>
                                <td align="center">
                                    <input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      				<input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />	
                                </td>
                                <td align="center">
                                    <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
                                        <i class="fa fa-trash-o"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <?php
            }
            ?>











			<br><br>


            
			<input type="text" name="nome" id="nome" title="Campo Nome" placeholder="SEU NOME" required="required"  />
			<input type="text" name="email" id="email" title="Campo E-mail" placeholder="SEU E-MAIL" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" />
			<input type="text" name="telefone" id="telefone" title="Campo Telefone" placeholder="SEU TELEFONE" required="required"  />
			
			<input type="text" name="empresa" id="empresa" title="Campo Empresa" placeholder="SUA EMPRESA" />
			<input type="text" name="area_atuacao" id="area_atuacao" title="Campo Área de atuação" placeholder="ÁREA DE ATUAÇÃO" />
			<input type="text" name="servico" id="servico" title="Campo Serviço" placeholder="SERVIÇO DESEJADO" required="required" />
			
			<input type="text" name="cidade" id="cidade" title="Campo Cidade" placeholder="CIDADE" required="required" />
			<input type="text" name="estado" id="estado" title="Campo Estado" placeholder="ESTADO" required="required" />
			
			<textarea name="mensagem" rows="5" id="mensagem" title="Campo Mensagem" required="required" placeholder="SUA MENSAGEM"></textarea>
			
			<div style="float:left;">
				<label>*Digite o que você vê na imagem abaixo.</label>
				<?php Util::gera_captcha("white"); ?>
			</div>
			
			<input type="submit" name="enviar" id="enviar" value="Enviar" title="Enviar"/>
		</form>
		
		
		
		
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>






<script language="javascript" type="application/javascript">
	///////////////////////////////////////////////////////////////////////////////////////////////
		// CONTATO
	////////////////////////////////////////////////////////////////////////////////////////////
	function validaContatos()
	{
		var form = document.getElementById("form_contato");
		var nome = document.getElementById("nome");	
		var email = document.getElementById("email");	
		var telefone = document.getElementById("telefone");
		var servico = document.getElementById("servico");
		var cidade = document.getElementById("cidade");
		var estado = document.getElementById("estado");
		var mensagem = document.getElementById("mensagem");
		
		
		if (nome.value == "" || nome.value == "NOME")
			alert('Por favor, informe seu nome.');
		else if (!checkMail(email.value))
			alert('E-mail inválido');
		else if (!checkMail(telefone.value))
			alert('Por favor, informe seu telefone');
		else if (!checkMail(servico.value))
			alert('Por favor, informe o serviço desejado.');
		else if (!checkMail(cidade.value))
			alert('Por favor, informe sua cidade.');
		else if (!checkMail(estado.value))
			alert('Por favor, informe seu estado.');
		else if (mensagem.value == "" || mensagem.value == "MENSAGEM")
			alert('Campo mensagem obrigatório');
		else
			form.submit();
		
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////
		// EMAIL
	////////////////////////////////////////////////////////////////////////////////////////////
	function checkMail(mail){
			var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
			if(typeof(mail) == "string"){
				if(er.test(mail)){ return true; }
			}else if(typeof(mail) == "object"){
				if(er.test(mail.value)){ 
							return true; 
						}
			}else{ 
				return false;
				}
	}
	

</script>