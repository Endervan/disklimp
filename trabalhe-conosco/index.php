﻿<?php
require_once("../class/Include.class.php"); 
require_once('../class/recaptchalib.php');
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="trabalhe_conosco">
		
		<div class="titulos">
			<h3>CONTATOS</h3>
			<h2>TRABALHE CONOSCO</h2>
		</div>
		
		<div class="content_navegacao_forms">
			<ul>
				<li><a href="<?php echo Util::caminho_projeto() ?>/contato/" title="FALE CONOSCO" class="fale_conosco">FALE CONOSCO</a></li>
				<li><a href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco" title="TRABALHE CONOSCO" class="trabalhe_conosco">TRABALHE CONOSCO</a></li>
				<li><a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ORÇAMENTOS" class="orcamentos">ORÇAMENTOS</a></li>
			</ul>
		</div>
		
		<div class="clear">&nbsp;</div>
		
		<?php  
		//	VERIFICO SE E PARA ENVIAR O EMAIL
		if(isset($_POST[nome]))
		{
			if (!Util::valida_captcha()) {
					Util::script_msg(utf8_decode("Por favor, digite o que você vê na imagem"));
					Util::script_go_back();
				} else {
					$nome_remetente = ($_POST[nome]);
					$assunto = "Contato pelo site $_SERVER[SERVER_NAME]";
					$email = ($_POST[email]);
					$mensagem = (nl2br($_POST[mensagem]));
					$telefone = ($_POST[telefone]);
					
					$escolaridade = ($_POST[escolaridade]);
					$area = ($_POST[area]);
					$cargo = ($_POST[cargo]);
					$cidade = ($_POST[cidade]);
					$estado = ($_POST[estado]);
					
					if(!empty($_FILES[curriculo]))
					{
						$curriculo = Util::upload_arquivo("../uploads/", $_FILES[curriculo]);
						$arquivo = Util::caminho_projeto()."/uploads/$curriculo";
						$texto_arquivo = "Curriculo: <a href=\"$arquivo\" target='_blank'>CLIQUE AQUI</a>";
					}
					
					
					$texto_mensagem = "
									  Nome: $nome_remetente <br />
									  Assunto: $assunto <br />
									  Telefone: $telefone <br />
									  Email: $email <br />
									  <br />
									  Escolaridade: $escolaridade<br />
									  Área: $area<br />
									  Cargo: $cargo<br />
									  Cidade: $cidade<br />
									  Estado: $estado<br />
									  <br />
									  $texto_arquivo<br />
									  Mensagem:	<br />
									  $mensagem
									  ";
										
					if(Util::envia_email($config[email],utf8_decode("Contato pelo site ").$_SERVER[SERVER_NAME], $texto_mensagem, utf8_decode($nome_remetente), $email));
                    Util::script_msg("Obrigado por entrar em contato.");

                                        
                                        /*
					if(Util::envia_email($config[email], "Contato pelo site $_SERVER[SERVER_NAME]", $texto_mensagem, $nome_remetente, $email))
					{
						Util::script_msg("Obrigado por entrar em contato.");
					}
					else
					{
						Util::script_msg("Houve um erro ao enviar o email.");
					}
                                         * 
                                         */
				}
		}
		?>
		
		<form id="form_contato" name="form_contato" enctype="multipart/form-data" method="post" action="#">
            	
			<input type="text" name="nome" id="nome" title="Campo Nome" placeholder="SEU NOME" required="required" />
			<input type="text" name="email" id="email" title="Campo E-mail" placeholder="SEU E-MAIL" required="required" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" />
			<input type="text" name="telefone" id="telefone" title="Campo Telefone" placeholder="SEU TELEFONE" required="required" />
			
			<input type="text" name="escolaridade" id="escolaridade" title="Campo Escolaridade" placeholder="SUA ESCOLARIDADE" required="required" />
			<input type="text" name="area" id="area" title="Campo Área" placeholder="ÁREA" />
			<input type="text" name="cargo" id="cargo" title="Campo Cargo" placeholder="CARGOS" />
			
			<input type="text" name="cidade" id="cidade" title="Campo Cidade" placeholder="CIDADE" required="required" />
			<input type="text" name="estado" id="estado" title="Campo Estado" placeholder="ESTADO" required="required" />
			<input type="file" name="curriculo" id="curriculo" title="Campo Currículo" required="required" />
			
			<textarea name="mensagem" rows="5" id="mensagem" title="Campo Mensagem" required="required" placeholder="SUA MENSAGEM"></textarea>
			
			<div style="float:left;">
				<label>*Digite o que você vê na imagem abaixo.</label>
				<?php Util::gera_captcha("white"); ?>
			</div>
			
			<input type="submit" name="enviar" id="enviar" value="Enviar" />
		</form>
		
		
		
		
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>