<!-- slider code start -->
<div id="content_slider">
	<div id="slider" class="royalSlider rsMinW">
		
		
		<div class="rsContent">
		
			<img style="margin:-169px -425px 0px 0px;" class="rsABlock" data-move-effect="right" src="<?php echo Util::caminho_projeto(); ?>/images/bg_slide01.png" alt="CONHEÇA A DISKLIMPEZA" width="970" height="519" />
			
			<div class="bContainer">
				<h1 class="rsABlock" data-move-effect="top">CONHEÇA A DISKLIMPEZA</h1>
				<ul>
					<li class="rsABlock" data-move-effect="left" data-delay="500" data-move-offset="50" data-fade-effect="true">TERCERIZAÇÃO DE MÃO DE OBRA</li>
					<li class="rsABlock" data-move-effect="left" data-delay="1000" data-move-offset="50" data-fade-effect="true">ADMINISTRAÇÃO DE CONDOMÍNIOS</li>
					<li class="rsABlock" data-move-effect="left" data-delay="1500" data-move-offset="50" data-fade-effect="true">JARDINAGEM</li>
					<li class="rsABlock" data-move-effect="left" data-delay="2000" data-move-offset="50" data-fade-effect="true">DEDETIZAÇÃO</li>
					<li class="rsABlock" data-move-effect="left" data-delay="2500" data-move-offset="50" data-fade-effect="true">LIMPESA EM GERAL</li>
				</ul>
				
				<a class="saiba_mais rsABlock" data-move-effect="left" data-delay="3000" data-move-offset="50" href="<?php echo Util::caminho_projeto(); ?>/disklimpeza" title="Saiba Mais">Saiba Mais</a>
			</div>
			<span class="rsTmb">Disklimpeza</span>
		</div>
		
		
		
		<div class="rsContent">
		
			<img style="margin:-169px -310px 0px 0px;" class="rsABlock" data-move-effect="right" src="<?php echo Util::caminho_projeto(); ?>/images/bg_slide02.png" alt="TERCERIZAÇÃO DE MÃO DE OBRA" width="884" height="519" />
			
			<div class="bContainer">
				<h2 class="rsABlock" data-move-effect="top">TERCERIZAÇÃO DE MÃO DE OBRA</h2>
				<p class="rsABlock" data-move-effect="bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make a typ</p>
				
				<a class="saiba_mais rsABlock" data-move-effect="left" data-delay="1200" data-move-offset="50" href="<?php echo Util::caminho_projeto(); ?>/disklimpeza" title="Saiba Mais">Saiba Mais</a>
			</div>
			<span class="rsTmb">Tercerização de mão <br />de obra</span>
		</div>
		
		
		
		<div class="rsContent">
		
			<img style="margin:-169px -295px 0px 0px;" class="rsABlock" data-move-effect="right" src="<?php echo Util::caminho_projeto(); ?>/images/bg_slide03.png" alt="ADMINISTRAÇÃO DE CONDOMÍNIOS" width="859" height="519" />
			
			<div class="bContainer">
				<h2 class="rsABlock" data-move-effect="top">ADMINISTRAÇÃO DE CONDOMÍNIOS</h2>
				<p class="rsABlock" data-move-effect="bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make a typ</p>
				
				<a class="saiba_mais rsABlock" data-move-effect="left" data-delay="1200" data-move-offset="50" href="<?php echo Util::caminho_projeto(); ?>/disklimpeza" title="Saiba Mais">Saiba Mais</a>
			</div>
			<span class="rsTmb">Administração de Condomínios</span>
		</div>
		
		
		
		
		<div class="rsContent">
		
			<img style="margin:-169px -250px 0px 0px;" class="rsABlock" data-move-effect="right" src="<?php echo Util::caminho_projeto(); ?>/images/bg_slide04.png" alt="PROMOÇÃO DE PRODUTOS DE LIMPEZA" width="859" height="519" />
			
			<div class="bContainer">
				<h2 class="rsABlock" data-move-effect="top">PROMOÇÃO DE PRODUTOS DE LIMPEZA</h2>
				<p class="rsABlock" data-move-effect="bottom">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make a typ</p>
				
				<a class="saiba_mais rsABlock" data-move-effect="left" data-delay="1200" data-move-offset="50" href="<?php echo Util::caminho_projeto(); ?>/disklimpeza" title="Saiba Mais">Saiba Mais</a>
			</div>
			<span class="rsTmb">Promoção de produtos <br />de limpeza</span>
		</div>
		
		
	</div>
</div>


<script id="addJS">
jQuery(document).ready(function($) {
	$('#slider').royalSlider({
		arrowsNav: false,
		arrowsNavAutoHide: false,
		fadeinLoadedSlide: false,
		controlNavigationSpacing: 0,
		controlNavigation: 'tabs',
		imageScaleMode: 'none',
		imageAlignCenter:false,
		blockLoop: true,
		loop: true,
		numImagesToPreload: 6,
		transitionType: 'fade',
		keyboardNavEnabled: true,
		//transitionSpeed:4000,
		autoPlay: {
    		// autoplay options go gere
    		enabled: true,
    		//pauseOnHover: true,
			delay: 4000
    	},
		block:
		{
			delay: 400
		}
	});
});
</script>
