<div id="content_topo">
    <div id="topo">

        <a href="<?php echo Util::caminho_projeto(); ?>/" title="Disklimp">
            <div class="logo">&nbsp;</div>
        </a>

        <ul id="menu">
            <li class="<?php echo $class_home ?>"><a href="<?php echo Util::caminho_projeto(); ?>/" title="HOME" class="home">HOME</a></li>
            <li class="<?php echo $class_empresa ?>"><a href="<?php echo Util::caminho_projeto(); ?>/empresa/" title="EMPRESA" class="empresa">EMPRESA</a></li>
            <li class="<?php echo $class_produtos ?>"><a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/" title="PRODUTOS" class="produtos">PRODUTOS</a></li>
            <li class="<?php echo $class_servicos ?>"><a href="<?php echo Util::caminho_projeto(); ?>/disklimpeza/" title="SERVIÇOS" class="disklimpeza">SERVIÇOS</a></li>
            <li class="<?php echo $class_dicas ?>"><a href="<?php echo Util::caminho_projeto(); ?>/dicas/" title="DICAS" class="dicas">DICAS</a></li>
            <li class="<?php echo $class_contato ?>"><a href="<?php echo Util::caminho_projeto(); ?>/contato" title="CONTATOS" class="contatos">CONTATOS</a></li>
        </ul>

    </div>
</div>

<div id="content_barra_info">
    <div id="barra_info">
        
        <div class="telefone">
            <p>TELEFONE</p>
            <a href="tel:+55<?php Util::imprime($config[telefone1]); ?>">
                <h5><?php Util::imprime($config[telefone1]); ?></h5>
            </a>
        </div>
        
        <div class="telefone">
            <p>TELEFONE</p>
            <a href="tel:+55<?php Util::imprime($config[telefone2]); ?>">
                <h5><?php Util::imprime($config[telefone2]); ?></h5>
            </a>
        </div>
        
        <a href="<?php echo Util::caminho_projeto(); ?>/orcamento" class="botao" title="ORÇAMENTOS">
            <div class="icon_orcamento">&nbsp;</div>
            ORÇAMENTOS
        </a>
        
        <a href="<?php echo Util::caminho_projeto(); ?>/contato" class="botao" title="COMO CHEGAR">
            <div class="icon_como_chegar">&nbsp;</div>
            COMO CHEGAR
        </a>
        
    </div>
</div>