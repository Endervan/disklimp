<script>
$(function() {
    $("#btn_filtro_marca").click(function() {
        $("#botoes_filtros a").removeClass("active");
        $("#btn_filtro_marca").addClass("active");
        $("#filtro_categoria").slideUp("fast");
        $("#filtro_marca").slideToggle();
    });
	
    /*$("#btn_filtro_categoria").click(function() {
        $("#botoes_filtros a").removeClass("active");
        $("#btn_filtro_categoria").addClass("active");
        $("#filtro_marca").slideUp("fast");
        $("#filtro_categoria").slideToggle();
    });*/
});
</script>

<form name="form_busca" id="form_busca" action="<?php echo Util::caminho_projeto(); ?>/produtos/" method="post">
    <input type="text" name="busca" id="busca" placeholder="Encontre o produto desejado" />
    <input type="submit" id="enviar" value="Enviar" />
</form>

<div id="content_filtros">
    
    <!-- FILTRO DE CATEGORIA -->
    <div id="filtro_categoria">
        <h1>SELECIONE A CATEGORIA:</h1>
        <ul>
            <?php
            $result = $obj_site->select("tb_categorias_produtos");
            if(mysql_num_rows($result) > 0)
            {
                while($row = mysql_fetch_array($result))
                {
                    ?>
                    <li>
                        <a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                            <img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
                        </a>
                        <p>
                            <a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                                <?php Util::imprime($row[titulo]); ?>
                            </a>
                        <p>
                    </li>
                    <?php
                }
                
            }
            ?>
        </ul>
    </div><!-- FIM FILTRO DE CATEGORIA -->
	
    <!-- FILTRO DE MARCA -->
    <div id="filtro_marca">
        <h1>SELECIONE A MARCA:</h1>
        <ul>
            <?php
            $result = $obj_site->select("tb_marcas_produtos");
            if(mysql_num_rows($result) > 0)
            {
                while($row = mysql_fetch_array($result))
                {
                    ?>
                    <li>
                        <a href="<?php echo Util::caminho_projeto(); ?>/produtos/marca/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                            <img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
                        </a>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
    </div><!-- FIM FILTRO DE MARCA -->
	
    <!-- BOTÕES FILTROS -->
    <div id="botoes_filtros">
        <h5>FILTRAR POR</h5>
        <a href="<?php echo Util::caminho_projeto(); ?>/produtos/" title="TODOS OS PRODUTOS">TODOS</a>
        <a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/" title="FILTRAR POR CATEGORIA">CATEGORIA</a>
        <a href="javascript:void(0);" id="btn_filtro_marca" title="FILTRAR POR MARCA">MARCA</a>
    </div><!-- FIM BOTÕES FILTROS -->
	
</div>