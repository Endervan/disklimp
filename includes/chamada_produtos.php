<div id="chamada_produtos">
	
	<h3 class="titulo">PRODUTOS</h3>
	
	<a id="prev" class="seta_voltar" href="#">&lt;</a>
	<a id="next" class="seta_avancar" href="#">&gt;</a>
	
	<div>
		<ul id="carrousel">
			<?php
			$result = $obj_site->select("tb_categorias_produtos","ORDER BY RAND() LIMIT 15");
			if(mysql_num_rows($result) > 0)
			{
				while($row = mysql_fetch_array($result))
				{
					?>
					<li>
						<a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
							<img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
							<p><?php Util::imprime($row[titulo]); ?></p>
						</a>
					</li>
					<?php
				}
			}
			?>
		</ul>
	</div>
	
</div>