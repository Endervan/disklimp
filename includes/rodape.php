<div id="content_rodape">
    <div id="rodape">
        <a href="<?php echo Util::caminho_projeto(); ?>/" title="Disklimp">	
            <div class="logo">&nbsp;</div>
        </a>
        
        <ul>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/" title="HOME">HOME</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/empresa/" title="EMPRESA">EMPRESA</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/produtos/categoria/" title="PRODUTOS">PRODUTOS</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/disklimpeza/" title="DISKLIMPEZA">SERVIÇOS</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/dicas/" title="DICAS">DICAS</a> |</li>
            <li><a href="<?php echo Util::caminho_projeto(); ?>/contato" title="CONTATOS">CONTATOS</a></li>
        </ul>
        
        <h4 class="endereco"><?php Util::imprime($config[endereco]); ?></h4>
        <a href="tel:+55<?php Util::imprime($config[telefone1]); ?>">
            <h4 class="telefone"><?php Util::imprime($config[telefone1]); ?></h4>
        </a>

        <?php if ($config[google_plus] != "") { ?>
            <a class="pull-left top25 left50" style="float: right; margin-top: -60px; color: #fff; margin-right: 95px;" href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
                <p class=""><i class="fa fa-google-plus"></i></p>
            </a>
        <?php } ?>
        
        <a href="http://www.homewebbrasil.com.br/" title="Homeweb Marketing" target="_blank">
            <div class="logo_homeweb">&nbsp;</div>
        </a>
        <p class="copyright">TODOS OS DIREITOS RESERVADOS DISKLIMP</p>
    </div>
</div>