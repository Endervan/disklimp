<!-- slider code start -->
<div id="content_slider">
	<div id="slider" class="royalSlider rsMinW">
		
		
		<div class="rsContent rsABlock" data-move-effect="left" data-move-offset="50">
		
			<img style="margin:-169px -425px 0px 0px;" src="<?php echo Util::caminho_projeto(); ?>/images/bg_slide01.png" alt="CONHEÇA NOSSOS SERVIÇOS" width="970" height="519" />
			
			<div class="bContainer">
				<h1>CONHEÇA NOSSOS SERVIÇOS</h1>
				<ul>
					<li>TERCEIRIZAÇÃO DE MÃO DE OBRA</li>
					<li>ADMINISTRAÇÃO DE CONDOMÍNIOS</li>
					<li>JARDINAGEM</li>
					<li>DEDETIZAÇÃO</li>
					<li>LIMPEZA EM GERAL</li>
				</ul>
				
				<a class="saiba_mais" href="<?php echo Util::caminho_projeto(); ?>/disklimpeza" title="Saiba Mais">Saiba Mais</a>
			</div>
			<span class="rsTmb">Serviços</span>
		</div>
		
		
		
		<div class="rsContent rsABlock" data-move-effect="right" data-move-offset="50">
		
			<img style="margin:-169px -310px 0px 0px;" src="<?php echo Util::caminho_projeto(); ?>/images/bg_slide02.png" alt="TERCEIRIZAÇÃO DE MÃO DE OBRA" width="884" height="519" />
			
			<div class="bContainer">
				<h2>TERCEIRIZAÇÃO DE MÃO DE OBRA</h2>
				<p>A empresa DISKLIMP oferece aos seus clientes variados tipos de serviços atendendo de forma rápida e eficiente as solicitações.</p>
				
				<a class="saiba_mais" href="<?php echo Util::caminho_projeto(); ?>/disklimpeza" title="Saiba Mais">Saiba Mais</a>
			</div>
			<span class="rsTmb">Tercerização de mão <br />de obra</span>
		</div>
		
		
		
		<div class="rsContent rsABlock" data-move-effect="left" data-move-offset="50">
		
			<img style="margin:-169px -295px 0px 0px;" src="<?php echo Util::caminho_projeto(); ?>/images/bg_slide03.png" alt="ADMINISTRAÇÃO DE CONDOMÍNIOS" width="859" height="519" />
			
			<div class="bContainer">
				<h2>ADMINISTRAÇÃO DE CONDOMÍNIOS</h2>
				<p>Equipe treinada e altamente qualificada, com serviços personalizados e soluções completas para condomínios verticais e horizontais.</p>
				
				<a class="saiba_mais" href="<?php echo Util::caminho_projeto(); ?>/disklimpeza" title="Saiba Mais">Saiba Mais</a>
			</div>
			<span class="rsTmb">Administração de Condomínios</span>
		</div>
		
		
		
		
		<div class="rsContent rsABlock" data-move-effect="right" data-move-offset="50">
		
			<img style="margin:-169px -250px 0px 0px;" src="<?php echo Util::caminho_projeto(); ?>/images/bg_slide04.png" alt="PROMOÇÃO DE PRODUTOS DE LIMPEZA" width="859" height="519" />
			
			<div class="bContainer">
				<h2>PRODUTOS PARA LIMPEZA</h2>
				<p>A DISKLIMP oferece uma linha completa de produtos de limpeza da mais alta qualidade e com preços competitivos.</p>
				
				<a class="saiba_mais" href="<?php echo Util::caminho_projeto(); ?>/disklimpeza" title="Saiba Mais">Saiba Mais</a>
			</div>
			<span class="rsTmb">Produtos <br />para limpeza</span>
		</div>
		
		
	</div>
</div>


<script id="addJS">
jQuery(document).ready(function($) {
	$('#slider').royalSlider({
		arrowsNav: false,
		arrowsNavAutoHide: false,
		fadeinLoadedSlide: false,
		controlNavigationSpacing: 0,
		controlNavigation: 'tabs',
		imageScaleMode: 'none',
		imageAlignCenter:false,
		blockLoop: true,
		loop: true,
		numImagesToPreload: 6,
		transitionType: 'fade',
		keyboardNavEnabled: true,
		//transitionSpeed:4000,
		autoPlay: {
    		// autoplay options go gere
    		enabled: true,
    		//pauseOnHover: true,
			delay: 4000
    	},
		block:
		{
			delay: 400
		}
	});
});
</script>
