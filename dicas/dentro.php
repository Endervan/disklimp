﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();

// INTERNA DE DICAS
$url = Util::trata_dados_formulario($_REQUEST[get1]);
if(!empty($url))
{
	$complemento = "AND url_amigavel = '".$url."'";
}

$result = $obj_site->select("tb_dicas",$complemento);
$row_dica = mysql_fetch_array($result);

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description($row_dica[description_google]);
$keywords = $obj_site->get_keywords($row_dica[keywords_google]);
$titulo_pagina = $obj_site->get_title($row_dica[description_google]);
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="dicas">
		
		<form name="form_busca" id="form_busca" action="<?php echo Util::caminho_projeto(); ?>/dicas/" method="post">
			<input type="text" name="busca" id="busca" placeholder="Encontre a dica desejada" />
			<input type="submit" name="enviar" id="enviar" value="Enviar" />
		</form>
		
		<div id="content_holder">
			<div id="content_principal">
				<p class="data"><?php echo Util::formata_data($row_dica[data]); ?></p>
				<h1><?php Util::imprime($row_dica[titulo]) ?></h1>
				
				<img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row_dica[imagem]) ?>" alt="<?php Util::imprime($row_dica[titulo]) ?>" />
				
				<?php Util::imprime($row_dica[descricao]); ?>
				
			</div>
			
			<div id="content_auxiliar">
				
				<div class="titulos">
					<h3>NOSSOS DESTAQUES DE</h3>
					<h2>PRODUTOS</h2>
				</div>
				
				<div class="clear">&nbsp;</div>
				
				<ul>
					<?php
					$result = $obj_site->select("tb_produtos","ORDER BY RAND() LIMIT 3");
					if(mysql_num_rows($result) > 0)
					{
						while($row = mysql_fetch_array($result))
						{
							?>
							<li>
								<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
									<img src="<?php echo Util::caminho_projeto(); ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
									<p><?php Util::imprime($row[titulo]); ?></p>
								</a>
								<a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" class="leia_mais" title="VER MAIS">
									VER MAIS
								</a>
							</li>
							<?php
						}
					}
					?>
					
				</ul>
				
				
			</div>
		</div>
		
		<div class="clear">&nbsp;</div>
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>
