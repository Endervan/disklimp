﻿<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$description = $obj_site->get_description("");
$keywords = $obj_site->get_keywords("");
$titulo_pagina = $obj_site->get_title("");
?>
<!doctype html>
<html>
<head>
	<?php include("../includes/head.php"); ?>
</head>

<body>

	<?php include("../includes/topo.php"); ?>
	
	<?php include("../includes/banners_internas.php"); ?>
	
	<div class="clear">&nbsp;</div>
	
	<div id="conteudo" class="dicas">
		
		<form name="form_busca" id="form_busca" action="<?php echo Util::caminho_projeto(); ?>/dicas/" method="post">
			<input type="text" name="busca" id="busca" placeholder="Encontre a dica desejada" />
			<input type="submit" name="enviar" id="enviar" value="Enviar" />
		</form>
		
		<ul id="lista_dicas">
			
			<?php
				if(isset($_POST[busca]))
				{
					// FILTRO DE DICAS
					$busca = Util::trata_dados_formulario($_POST[busca]);
					if(!empty($busca))
					{
						$complemento = "AND titulo like '%$busca%'";
					}
				}
			
				$result = $obj_site->select("tb_dicas",$complemento);
				if(mysql_num_rows($result) > 0)
				{
					while($row = mysql_fetch_array($result))
					{
						?>
						<li>
							<a href="<?php echo Util::caminho_projeto(); ?>/dicas/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
								<p class="data"><?php echo Util::formata_data($row[data]); ?></p>
								<img src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" />
								<p><?php Util::imprime($row[titulo]); ?></p>
							</a>
						</li>
						<?php
					}
				}
				else
				{
					?>
					<h1>Nenhum Produto Encontrado</h1>
					<?php
				}
				?>
			
			
			<?php /*?>
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica01.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry ting industry industry.</p>
				</a>
			</li>
			
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica02.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry.</p>
				</a>
			</li>
			
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica03.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry.</p>
				</a>
			</li>
			
			
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica01.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry.</p>
				</a>
			</li>
			
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica02.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry.</p>
				</a>
			</li>
			
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica03.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry.</p>
				</a>
			</li>
			
			
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica01.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry.</p>
				</a>
			</li>
			
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica02.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry.</p>
				</a>
			</li>
			
			<li>
				<a href="#" title="Lorem Ipsum is simply dummy text of the printing and typeset ting industry.">
					<p class="data">10/10/2013</p>
					<img src="<?php echo Util::caminho_projeto(); ?>/images/exemplos/tumb_dica03.jpg" alt="Lorem Ipsum is simply dummy text of the printing and typeset ting industry." />
					<p>Lorem Ipsum is simply dummy text of the printing and typeset ting industry.</p>
				</a>
			</li>
			<?php */?>
			
		</ul>
		
		<div class="clear">&nbsp;</div>
		
	</div>
	
	<?php include("../includes/rodape.php"); ?>
	
</body>
</html>
